package biz.niggemann;

import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import static biz.niggemann.FadownUtility.logItOut;

class FadownFaceFeaturesDetection {

    private int featureImageMaxSize = -1;
    private boolean showOnlyFaces = false;
    private boolean generateFeatureImagesOnly = false;
    final CascadeClassifier eyeCascadesClassifier;
    final CascadeClassifier frontalFaceCascadesClassifier;
    final CascadeClassifier profileFaceCascadesClassifier;

    // Constructor
    FadownFaceFeaturesDetection(int maxSize, boolean facesOnly, boolean onlyGenerateDetection) {
        try { // Load openCV library
            System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
            featureImageMaxSize = maxSize;
            showOnlyFaces = facesOnly;
            generateFeatureImagesOnly = onlyGenerateDetection;
        } catch (UnsatisfiedLinkError e) {
            logItOut("Failed to load openCV library.\n" +
                    "Use -Djava.library.path= at start of JVM and add the path to your openCV library.\n" +
                    "Example: java -Djava.library.path=C:\\opencv\\build\\java\\x64 -jar ...", 0);
        }

        // Load XML files for detection of face features
        final String cascadesPath = "/cascades/";
        eyeCascadesClassifier = xmlToCascadeClassifier(cascadesPath + "haarcascade_eye.xml");
        frontalFaceCascadesClassifier = xmlToCascadeClassifier(cascadesPath + "haarcascade_frontalface_alt.xml");
        profileFaceCascadesClassifier = xmlToCascadeClassifier(cascadesPath + "haarcascade_profileface.xml");

        if ((eyeCascadesClassifier == null) || (frontalFaceCascadesClassifier == null) || (profileFaceCascadesClassifier == null)) //
            featureImageMaxSize = -1; // not all cascades loaded, no face feature display
    }

    private CascadeClassifier xmlToCascadeClassifier(String xmlFile) {
        CascadeClassifier cascadeClassifier = null;

        try {
            Path tmpXml = Files.createTempFile("xml_", "cv");
            InputStream inputStreamXml = FadownFaceFeaturesDetection.class.getResourceAsStream(xmlFile);
            if (inputStreamXml != null) {
                Files.copy(inputStreamXml, tmpXml, StandardCopyOption.REPLACE_EXISTING);
                cascadeClassifier = new CascadeClassifier(tmpXml.toString());
            } else
                logItOut("XML file " + xmlFile + " for openCV missing.", 2);
        } catch (IOException e) {
            logItOut("XML file " + xmlFile + " for openCV not created.", 2);
        }

        return cascadeClassifier;
    }

    void showFaceFeatures(String imageFilename) {
        if (featureImageMaxSize > 0) {
            Mat image = Imgcodecs.imread(imageFilename); // Input image
            List<Rect> faceFeatureList = new ArrayList<>();

            if (!showOnlyFaces) { // Detect eyes
                getFaceFeaturesOrFaces(faceFeatureList, image, eyeCascadesClassifier);
            }

            if (faceFeatureList.isEmpty()) // No eyes detected, collect faces instead
                getFaceFeaturesOrFaces(faceFeatureList, image, frontalFaceCascadesClassifier);

            if (faceFeatureList.isEmpty()) // No frontal faces detected, collect profil faces instead
                getFaceFeaturesOrFaces(faceFeatureList, image, profileFaceCascadesClassifier);

            if (!faceFeatureList.isEmpty()) { // Something found
                List<String> filenameList = generateImagesResizedRectangles(image, imageFilename, faceFeatureList, this.featureImageMaxSize);
                if (!generateFeatureImagesOnly)
                    filenameList.forEach(FadownFileUtility::showImageFile);
            }
        }
    }

    private void getFaceFeaturesOrFaces(List<Rect> featureList, Mat image, CascadeClassifier faceFeatureDetector) {
        MatOfRect faceFeatureDetections = new MatOfRect();
        faceFeatureDetector.detectMultiScale(image, faceFeatureDetections);

        for (Rect rect : faceFeatureDetections.toArray()) {
            Rect faceFeatureRectangle = new Rect(rect.x, rect.y, rect.width, rect.height);
            featureList.add(faceFeatureRectangle);
        }
    }

    private List<String> generateImagesResizedRectangles(Mat image, String imageFilename, List<Rect> featureList, int targetSize) {
        List<String> filenameList = new ArrayList<>();
        if (!featureList.isEmpty()) // Something to show
        {
            File f = new File(imageFilename);
            String baseNameInclExtension = f.getName();
            String filePath = f.getParent();
            String[] baseNameElements = baseNameInclExtension.split("\\.(?=[^\\.]+$)");
            String baseName = baseNameElements[0];
            String newFilenameBase = filePath + File.separator + baseName + "_face_feature";

            featureList.forEach(cropRectangle -> {
                Mat markedImage = new Mat(image, cropRectangle);
                Mat resizedImage = new Mat();
                double factor = Double.min((double) targetSize / cropRectangle.width, 4.0);
                Size resizeSize = new Size(markedImage.rows() * factor, markedImage.rows() * factor);
                Imgproc.resize(markedImage, resizedImage, resizeSize, 0, 0, Imgproc.INTER_LANCZOS4);
                String rectSpec = "_" + cropRectangle.x + "_" + cropRectangle.y + "_" + cropRectangle.width + "_" + cropRectangle.height;
                String filename = newFilenameBase + rectSpec + ".jpg";
                Imgcodecs.imwrite(filename, resizedImage);
                filenameList.add(filename);
            });
        }
        return filenameList;
    }

}
