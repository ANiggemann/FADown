package biz.niggemann;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataException;
import com.drew.metadata.exif.ExifDirectoryBase;
import com.drew.metadata.exif.ExifIFD0Directory;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static biz.niggemann.FadownUtility.*;
import static java.awt.event.KeyEvent.*;

class FadownFileUtility {

    private static final String SEMAPHORE_FILE = endsPathWithFileSeparator(System.getProperty("java.io.tmpdir")) + "FADown.stop";
    private static final String ERROR_SIGNAL = "ERROR";
    private static final String EXIT_SIGNAL = "EXIT";
    private static final String OK_SIGNAL = "OK";
    private static final String CSV_DELIMITER = ",";
    private static JFrame frame = null;
    private static final ArrayList<String> imageFilenameList = new ArrayList<>();
    private static final HashMap<String, Integer> starRatingMap = new HashMap<>();
    static String lastImageFilename = "";
    private static String displayedImageFilename = "";
    private static boolean showExtraInfo = false;
    private static FadownFaceFeaturesDetection faceFeaturesDetection;

    private FadownFileUtility() {
        throw new IllegalStateException("Utility class");
    }

    static void fileUtilityStart(FadownParameters p) {
        if (p.getDetectionMaxSize() > 0) // Load everything for openCV face features detection
            faceFeaturesDetection = new FadownFaceFeaturesDetection(p.getDetectionMaxSize(), p.isDetectFacesOnly(), p.isGenerateDetectionOnly());
        loadStarRatings(p.getRatingsFilename());
    }

    static void showImageFileConditionally(boolean condition, String imageFilename) {
        if (condition && (imageFilename.length() > 0)) {
            if (faceFeaturesDetection != null)
                faceFeaturesDetection.showFaceFeatures(imageFilename);
            showImageFile(imageFilename);
        }
        lastImageFilename = "";
    }

    static void showImageFile(String imageFilename) {
        if (p.isInternalViewer())
            showImageFileInternal(imageFilename);
        else {
            try {
                if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                    URI fileURI = new URI("file:///" + imageFilename.replace("\\", "/"));
                    Desktop.getDesktop().browse(fileURI);
                }
            } catch (URISyntaxException | IOException e) {
                logStackTrace(e);
            }
        }
    }

    static int convertEXIFOrientationToAngle(String imageFilename) {
        int angle;
        int orientation = 1;
        try {
            Metadata metadata = ImageMetadataReader.readMetadata(new File(imageFilename));
            Directory directory = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
            orientation = directory.getInt(ExifDirectoryBase.TAG_ORIENTATION);
        } catch (MetadataException | IOException | ImageProcessingException e) {
        }
        switch (orientation) {
            case 3: angle = 180; break;
            case 6: angle = 90; break;
            case 8: angle = 270; break;
            default: angle = 0;
        }
        return angle;
    }

    static BufferedImage rotateImage(BufferedImage img, double angle) {
        double sin = Math.abs(Math.sin(Math.toRadians(angle)));
        double cos = Math.abs(Math.cos(Math.toRadians(angle)));
        int w = img.getWidth();
        int h = img.getHeight();
        int neww = (int)Math.floor(w * cos + h * sin);
        int newh = (int)Math.floor(h * cos + w * sin);
        BufferedImage bimg = new BufferedImage(neww, newh, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = bimg.createGraphics();
        g.translate((neww - w) / 2, (newh - h) / 2);
        g.rotate(Math.toRadians(angle), (float)w / 2, (float)h / 2);
        g.drawRenderedImage(img, null);
        g.dispose();
        return bimg;
    }

    static void previousImageInList() {
        if (imageFilenameList.size() > 1) {
            int idxFilename = imageFilenameList.indexOf(displayedImageFilename);
            String fname = (idxFilename > 0) ? imageFilenameList.get(idxFilename-1) : imageFilenameList.get(imageFilenameList.size()-1) ;
            showImageFileInternal(fname);
        }
    }

    static void nextImageInList() {
        if (imageFilenameList.size() > 1) {
            int idxFilename = imageFilenameList.indexOf(displayedImageFilename);
            String fname = (idxFilename < imageFilenameList.size()-1) ? imageFilenameList.get(idxFilename+1) : imageFilenameList.get(0);
            showImageFileInternal(fname);
        }
    }

    static void processKey(int keyCode) {
        switch (keyCode) {
            case VK_ESCAPE:
                System.exit(0);
                break;
            case VK_LEFT:
                previousImageInList();
                break;
            case VK_RIGHT:
                nextImageInList();
                break;
            case VK_HOME:
                showImageFileInternal(imageFilenameList.get(0));
                break;
            case VK_END:
                showImageFileInternal(imageFilenameList.get(imageFilenameList.size() - 1));
                break;
            case VK_F1:
                showExtraInfo = !showExtraInfo;
                showImageFileInternal(displayedImageFilename);
                break;
            case VK_F5:
                if (p.getF5ExternalProgram().length() > 0)
                    startExternalProgram(p.getF5ExternalProgram(), displayedImageFilename);
                break;
            case VK_F6:
                if (p.getF6ExternalProgram().length() > 0)
                    startExternalProgram(p.getF6ExternalProgram(), displayedImageFilename);
                break;
            case VK_F7:
                if (p.getF7ExternalProgram().length() > 0)
                    startExternalProgram(p.getF7ExternalProgram(), displayedImageFilename);
                break;
            case VK_F8:
                if (p.getF8ExternalProgram().length() > 0)
                    startExternalProgram(p.getF8ExternalProgram(), displayedImageFilename);
                break;
            case VK_0: saveStarRating(displayedImageFilename, 0); break;
            case VK_1: saveStarRating(displayedImageFilename, 1); break;
            case VK_2: saveStarRating(displayedImageFilename, 2); break;
            case VK_3: saveStarRating(displayedImageFilename, 3); break;
            case VK_4: saveStarRating(displayedImageFilename, 4); break;
            case VK_5: saveStarRating(displayedImageFilename, 5); break;
            default: break;
        }
    }

    static void initFrame() {
        frame = new JFrame("FADown");
        frame.setUndecorated(true);
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gs = ge.getDefaultScreenDevice();
        gs.setFullScreenWindow(frame);
        frame.setFocusable(true);
        frame.requestFocusInWindow();
        frame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                processKey(e.getKeyCode());
            }
        });
    }

    static void loadStarRatings(String ratingsFilename) {
        starRatingMap.clear();
        List<String> oldfileContent = null;
        Path starPath = Paths.get(ratingsFilename);
        if (Files.exists(starPath)) {
            try {
                oldfileContent = Files.readAllLines(starPath, Charset.defaultCharset());
            } catch (IOException e) {
                logItOut(ratingsFilename + " exists but not readable", 2);
            }
        }
        if (oldfileContent != null) {
            for (String actuLine : oldfileContent) {
                String[] ratingElements = actuLine.split("\\s*" + CSV_DELIMITER + "\\s*");
                if (ratingElements.length == 2) {
                    String imageFilename = ratingElements[0].trim();
                    int rating;
                    try {
                        rating = Integer.parseInt(ratingElements[1]);
                        starRatingMap.put(imageFilename, rating);
                    } catch (NumberFormatException e) {
                        logStackTrace(e);
                    }
                }
            }
        }
    }

    static void saveStarRating(String imageFilename, int starRating) {
        String imgFilename = Paths.get(imageFilename).getFileName().toString();
        starRatingMap.remove(imgFilename);
        if (starRating > 0)
            starRatingMap.put(imgFilename, starRating);
        ArrayList<String> newfileContent = new ArrayList<>();
        starRatingMap.forEach((fname, rating) -> newfileContent.add(fname + CSV_DELIMITER + rating));
        try {
            Files.write(Paths.get(p.getRatingsFilename()), newfileContent, Charset.defaultCharset());
        } catch (IOException e) {
            logStackTrace(e);
        }
        showExtraInfo = true;
        showImageFileInternal(displayedImageFilename);
    }

    static void addInfoToImage(Graphics g, String imageFilename) {
        if (showExtraInfo) {
            int idxFilename = imageFilenameList.indexOf(displayedImageFilename) + 1;
            String imgFilename = Paths.get(imageFilename).getFileName().toString();
            String fileInfoString = idxFilename + ": " + imgFilename;
            if (starRatingMap.containsKey(imgFilename))
                fileInfoString += " " + new String(new char[starRatingMap.get(imgFilename)]).replace("\0", "*");
            g.setColor(Color.WHITE);
            g.setFont(g.getFont().deriveFont(30f));
            g.drawString(fileInfoString, 10, frame.getHeight() - 10);
        }
    }

    static void drawAdjustedPhoto(BufferedImage img, int angle, Graphics g, ImageObserver target) {
        int shrinkFactorVertically = (showExtraInfo) ? 40 : 0;
        BufferedImage imgRotated = (angle != 0) ? rotateImage(img, angle) : img;
        float rateX = (float)frame.getWidth() / (float)imgRotated.getWidth();
        float rateY = ((float)frame.getHeight() - shrinkFactorVertically) / (float)imgRotated.getHeight();
        int w = (int)(imgRotated.getWidth() * rateY);
        int h = (int)(imgRotated.getHeight() * rateY);
        if (rateX <= rateY) {
            w = (int)(imgRotated.getWidth() * rateX);
            h = (int)(imgRotated.getHeight() * rateX);
        }
        int l = (frame.getWidth() - w) / 2;
        int t = ((frame.getHeight() - h) / 2) - (shrinkFactorVertically / 2);
        g.setColor(Color.BLACK);
        g.fillRect(0,0,frame.getWidth(),frame.getHeight());
        g.drawImage(imgRotated, l, t, w, h, target);
    }

    static void showImageFileInternal(String imageFilename) {
        try {
            BufferedImage img = ImageIO.read(new File(imageFilename));
            if (img != null) {
                if (frame == null) { // do it only at the first image
                    initFrame();
                } else {
                    frame.getContentPane().removeAll(); // Remove last image if there
                }
                frame.add(new Component() {
                    @Override
                    public void paint(Graphics g) {
                        super.paint(g);
                        if (!imageFilenameList.contains(imageFilename))
                            imageFilenameList.add(imageFilename);
                        displayedImageFilename = imageFilename;
                        int angle = convertEXIFOrientationToAngle(imageFilename);
                        drawAdjustedPhoto(img, angle, g, this);
                        addInfoToImage(g, imageFilename);
                    }
                });
                frame.validate();
            }
        } catch (IOException e) {
            logItOut(imageFilename + " not available", 2);
        }
    }

    static boolean fileExtensionMatchesProcessList(FadownParameters p, String filename) {
        boolean retVal = true;
        if (p.getProcessFileTypes().length() > 0) {
            String extension = filename.substring(filename.lastIndexOf('.') + 1).toUpperCase();
            retVal = (p.getProcessFileTypes() + ',').contains(extension + ',');
        }
        return retVal;
    }

    static void makeBackups(String fileToWrite, List<String> folderList) {
        folderList.parallelStream().forEach(folderName -> {
            folderName = (folderName.endsWith(File.separator)) ? folderName : folderName + File.separator;

            if ((folderName.length() > 1) && (destinationDirectoryExists(folderName))) {
                try {
                    String targetFileName = folderName + new File(fileToWrite).getName();
                    Path target = Paths.get(targetFileName);
                    Path originalPath = Paths.get(fileToWrite);
                    Files.copy(originalPath, target, StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) { // Suppress Exceptions
                }
            }
        });
    }

    // Force creation of destination directory
    static boolean destinationDirectoryExists(String directoryName) {
        boolean retVal = false;
        directoryName += File.separator;
        directoryName = directoryName.replace(File.separator + File.separator, File.separator); // eliminate double file separators
        File dir = new File(directoryName);

        if (!dir.exists()) // Create destination directory if not there
            try {
                logItOut("Creating destination folder " + directoryName, 2);
                Files.createDirectories(Paths.get(directoryName));
            } catch (IOException e) {
                logStackTrace(e);
            }

        if (!dir.exists()) // Still destination directory not there -> Error
            logItOut("Destination folder " + directoryName + " does not exist and could not be created", 0);
        else
            retVal = true;

        return retVal;
    }

    static String removeDoubleSlashes(String path) {
        String retVal = path.replace("//", "/");
        return retVal.replace("http:/", "http://");
    }

    private static String endsPathWithFileSeparator(String path) {
        return (path.endsWith(File.separator)) ? path : path + File.separator;
    }

    static String getTempDir() {
        return endsPathWithFileSeparator(System.getProperty("java.io.tmpdir"));
    }

    static String getSemaphoreFile() {
        return SEMAPHORE_FILE;
    }

    static String getErrorSignal() {
        return ERROR_SIGNAL;
    }

    static String getOkSignal() {
        return OK_SIGNAL;
    }

    static String getExitSignal() {
        return EXIT_SIGNAL;
    }


}
