package biz.niggemann;

import java.util.Arrays;
import java.util.List;

import static biz.niggemann.FadownFileUtility.*;
import static biz.niggemann.FadownHtmlProcessing.*;
import static biz.niggemann.FadownUtility.*;

public class Main {

    public static void main(String[] args) {
        int returnCode = 0;

        utilityStart(args);

        if (!exitWasSignaled()) {
            if (p.getImageFilenamesToShow().length() > 0) {
                List<String> filenameList = Arrays.asList(p.getImageFilenamesToShow().split(","));
                filenameList.forEach(filename -> showImageFileConditionally(true, filename));
                while (p.isRunContinuously()) {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException ex) { }
                }
            } else
                returnCode = ((resolveSourceURL(p).length() > 0) && (p.getDestinationFolder().length() > 0)) ? processIt() : 0;
        }

        System.exit(returnCode); // returns number of downloaded files as return code
    }

    // Main processing loop
    private static int processIt() {
        int retVal = 0;

        if (destinationDirectoryExists(p.getDestinationFolder()))
            do {
                try { Thread.sleep(200); } catch (InterruptedException ex) { }
                int rc = downloadFiles(resolveSourceURL(p), p.getDestinationFolder(), p.getFileTypes() + ",");
                if (rc > 0) { // something was downloaded
                    showImageFileConditionally(p.isShowLastImageInBrowser(), lastImageFilename);
                    retVal += rc;
                }
                if (p.isRunContinuously())
                    doWait(p.getPauseTime(), true); // pause if pause time is set
            }
            while (p.isRunContinuously()); // runContinuously can be set to false (= exit loop) by stop signal from other instance

        return retVal;
    }

    // Download and count files
    private static int downloadFiles(String durl, String lclfolder, String fitypes) {
        int retVal = 0;
        durl = (durl.toUpperCase().indexOf("HTTP://") == 0) ? durl : "http://" + durl; // add http:// if missing
        String htmlContent = readHtmlFromUrl(durl, true);

        if (!htmlContent.equals(getErrorSignal()) && !htmlContent.equals(getExitSignal())) {
            StringBuilder sb = new StringBuilder();
            // Get list of image files in base folder
            sb.append(getFileListFromHtml(htmlContent, durl + "/", fitypes)).append(",");
            // Get list of Folders
            String folderList = getFileListFromHtml(htmlContent, "", fitypes);
            String[] folders = folderList.split(",");

            for (String flashFolder : folders) {
                String furl = durl + "/" + flashFolder;
                String str = readHtmlFromUrl(furl, true);
                if (!str.equals(getErrorSignal()) && !str.equals(getExitSignal()))
                    sb.append(getFileListFromHtml(str, furl + "/", fitypes)).append(",");
                else {
                    sb.setLength(0);
                    break;
                }
            }

            retVal = processFilesList(sb.toString(), lclfolder);
        }

        return retVal;
    }

    private static int processFilesList(String fileList, String lcldest) {
        int retVal = 0;

        if (fileList.length() > 1) {
            logItOut("File list: " + fileList, 2);
            retVal = (p.isDownloadParallel()) ? downloadParallel(fileList, lcldest) : downloadSequential(fileList, lcldest);
        }

        return retVal;
    }

}
