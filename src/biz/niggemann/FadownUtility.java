package biz.niggemann;

import java.io.*;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

import static biz.niggemann.FadownFileUtility.*;
import static biz.niggemann.FadownOverlay.overlayImageWithPng;
import static biz.niggemann.FadownOverlay.overlayStart;
import static java.lang.Thread.sleep;

class FadownUtility {

    static FadownParameters p;

    private FadownUtility() {
        throw new IllegalStateException("Utility class");
    }

    static void utilityStart(String[] args) {
        p = new FadownParameters(args);
        fileUtilityStart(p);
        overlayStart(p);
    }

    private static String readBinaryFileFromUrl(URL actuUrl, String fileToWrite) throws IOException {
        String retVal = getErrorSignal();
        final int bufferLength = 8192;
        final int i = fileToWrite.lastIndexOf('.');

        if ((i == -1) || (i + 1 >= fileToWrite.length())) {
            logItOut("File extension missing: " + fileToWrite, 2);
        } else {
            final String tempFilename = fileToWrite.substring(0, i) + ".$$$"; // change file extension
            logItOut("Temp filename: " + tempFilename, 2);

            try (BufferedInputStream bInputStream = new BufferedInputStream(actuUrl.openStream())) {
                try (FileOutputStream fOutStream = new FileOutputStream(tempFilename)) { // Download file into temp file
                    byte[] buffer = new byte[bufferLength];
                    int count;
                    while ((count = bInputStream.read(buffer, 0, bufferLength)) != -1)
                        fOutStream.write(buffer, 0, count);
                }
            }

            retVal = storeAndViewAndProcessDownloadedFile(tempFilename, fileToWrite);
        }

        return retVal;
    }

    private static String storeAndViewAndProcessDownloadedFile(String tempFilename, String fileToWrite) {
        String retVal = getErrorSignal();
        final File tempFile = new File(tempFilename);

        if (tempFile.exists()) {
            File destinationFile = new File(fileToWrite);
            retVal = (tempFile.renameTo(destinationFile)) ? getOkSignal() : getErrorSignal(); // Rename temp file to destination file

            if (retVal.equals(getOkSignal())) {
                processDownloadedImage(fileToWrite);
                logItOut(fileToWrite, 1);
            }
        }

        return retVal;
    }

    private static void processDownloadedImage(String filename) {
        if (p.getBackupFolderList().length() > 0)
            makeBackups(filename, Arrays.asList(p.getBackupFolderList().split(",")));

        if (fileExtensionMatchesProcessList(p, filename)) {
            if (p.getPngOverlayFilename().length() > 0)
                overlayImageWithPng(filename);

            if (p.getExternalProgram().length() > 0)
                startExternalProgram(p.getExternalProgram(), filename);

            showImageFileConditionally(p.isShowAllImagesInBrowser(), filename);
        }
    }

    static void startExternalProgram(String externalProgram, String imageFilename) {
        boolean isWindows = System.getProperty("os.name").toUpperCase().startsWith("WINDOWS");
        String shellStart = (isWindows) ? "cmd.exe /c" : "sh -c";

        try {
            Runtime.getRuntime().exec(String.format("%s %s \"%s\"", shellStart, externalProgram, imageFilename));
        } catch (IOException e) { // Suppress Exceptions
        }
    }

    private static int downloadSingleFile(String urlFname, String lcldest) {
        int retVal = 0;
        int retryCounter = 5;

        String[] parts = urlFname.split("/");
        String fnameWithoutPath = parts[parts.length - 1]; // Get filename only
        String fname = lcldest + File.separator + fnameWithoutPath;
        if ((fnameWithoutPath.length() > 1) && (fnameWithoutPath.lastIndexOf('.') >= 1)) { // Only download file (image) with extension
            File f = new File(fname);

            if (!f.exists()) {
                while (retryCounter > 0) {
                    try {

                        logItOut("Read URL: " + urlFname, 2);
                        logItOut("Destination: " + fname, 2);

                        String success = readBinaryFileFromUrl(new URL(urlFname), fname);
                        if (success.equals(getExitSignal()))
                            retVal = -1; // Break loop because of stop signaled
                        if (success.equals(getOkSignal())) {
                            retVal++;
                            lastImageFilename = fname;
                            doWait(p.getWaitAfterDownloadTime(), true);
                        } else
                            retVal = -2; // Break loop because of error

                        retryCounter = 0;
                    } catch (IOException e) {
                        retryCounter = notConnectedErrorMessage(retryCounter);
                        retVal = -2;
                    }
                }
            }
        }

        return retVal;
    }

    // Copy files from FlashAir card to local folder according to the list
    static int downloadSequential(String flist, String lcldest) {
        int retVal = 0;
        flist = removeDoubleSlashes(flist);
        final String[] fl = flist.split(",");

        for (String urlFname : fl) {
            if (downloadSingleFile(urlFname, lcldest) < 0)
                break;
            else
                retVal++;
        }

        return retVal;
    }

    static int downloadParallel(String flist, String lcldest) {
        int retVal = 0;
        flist = removeDoubleSlashes(flist);
        final List<String> fl = Arrays.asList(flist.split(","));

        if (!fl.isEmpty()) {
            ForkJoinPool forkJoinPool = new ForkJoinPool(4);

            try {
                List<Integer> countList = forkJoinPool.submit(() -> processFilesParallel(fl, lcldest)).get();

                for (int count : countList) {
                    if (count < 0)
                        break;
                    else
                        retVal += count;
                }
            } catch (ExecutionException | InterruptedException e) {
                logStackTrace(e);
                Thread.currentThread().interrupt();
            } finally {
                forkJoinPool.shutdown();
            }
        }

        return retVal;
    }

    private static List<Integer> processFilesParallel(List<String> flist, String destination) {
        return flist.parallelStream().map(filename -> downloadSingleFile(filename, destination)).collect(Collectors.toList());
    }

    static void doWait(int waitingTimeInSeconds, boolean allowExit) {
        if (waitingTimeInSeconds > 0)
            try {
                for (int i = 0; i < waitingTimeInSeconds; i++) {
                    if (allowExit && exitWasSignaled())
                        break;
                    else
                        sleep(1000L);
                }
            } catch (InterruptedException e) {
                logStackTrace(e);
                Thread.currentThread().interrupt();
            }
    }

    static void logStackTrace(Exception e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        String exceptionAsString = sw.toString();
        System.out.println(exceptionAsString);
    }

    static void logItOut(String logString, int logLevel) {
        if (p.getLogIt() >= logLevel)
            System.out.println(logString);
    }

    static void stopAllInstances() {
        boolean writeSuccessfull = false;
        try (PrintWriter out = new PrintWriter(getSemaphoreFile())) {
            out.println("STOP");
            writeSuccessfull = true;
        } catch (FileNotFoundException e) {
            logStackTrace(e);
        }
        if (writeSuccessfull)
            doWait(15, false);
    }

    static boolean exitWasSignaled() {
        boolean retVal = false;
        File f = new File(getSemaphoreFile());

        if (f.exists()) {
            f.deleteOnExit(); // delete semaphore file at exit
            if (p != null)
                p.setRunContinuously(false);
            retVal = true;
        }

        return retVal;
    }

    static int notConnectedErrorMessage(int retryc) {
        if (retryc <= 1) {
            logItOut("FlashAir Card not connected", 1);
            doWait(10, true);
        }
        return retryc--;
    }
}
