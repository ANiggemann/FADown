package biz.niggemann;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.Enumeration;
import java.util.regex.Pattern;

import static biz.niggemann.FadownFileUtility.getErrorSignal;
import static biz.niggemann.FadownFileUtility.getExitSignal;
import static biz.niggemann.FadownUtility.*;

class FadownHtmlProcessing {

    private FadownHtmlProcessing() {
        throw new IllegalStateException("FadownUtility class");
    }

    // Read HTML page from URL
    static String readHtmlFromUrl(String readThisURL, boolean normalTimeouts) {
        String retVal = getExitSignal();
        int retryCounter = 5;

        if (!exitWasSignaled()) {
            while (retryCounter > 0) {
                try {
                    URL urlToRead = new URL(readThisURL);
                    logItOut("Read URL: " + readThisURL, 2);
                    URLConnection c = urlToRead.openConnection();
                    if (normalTimeouts) {
                        c.setConnectTimeout(5000);
                        c.setReadTimeout(10000);
                    } else { // Fast timeouts for scans
                        c.setConnectTimeout(500);
                        c.setReadTimeout(500);
                    }

                    try (BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()))) {
                        String inputLine;
                        StringBuilder sb = new StringBuilder();
                        while ((inputLine = in.readLine()) != null)
                            sb.append(inputLine).append("\n");
                        retVal = sb.toString();
                    }
                    retryCounter = 0;
                } catch (IOException e) {
                    if (normalTimeouts) {
                        retryCounter = notConnectedErrorMessage(retryCounter);
                        retVal = getErrorSignal();
                    } else {
                        retryCounter = 0;
                    }
                }
            }
        }

        return retVal;
    }

    private static String processHTMLLine(String htmlLine, String prefix, String fitypes) {
        String retVal = "";
        final String[] lparts = htmlLine.split("\"");

        if (lparts.length > 7) {
            String element = lparts[7].trim();
            if (element.length() > 0) {
                String[] fnameParts = element.split(Pattern.quote(".")); // Get filename and file type
                String ext = fnameParts[fnameParts.length - 1].toUpperCase(); // file type
                if ((fnameParts.length == 1) || (fitypes.length() < 2) || (fitypes.contains(ext + ","))) // file type OK
                    retVal = prefix + element + ",";
            }
        }

        return retVal;
    }

    // Extract the file list from the HTML string
    static String getFileListFromHtml(String htmls, String prefix, String fitypes) {
        String retVal;
        StringBuilder sb = new StringBuilder();
        final String[] htmlLines = htmls.split("\\n");

        for (String line : htmlLines) {
            if (line.contains("/DCIM") && !line.contains("__TSB")) // photo folder or photo file name
                sb.append(processHTMLLine(line, prefix, fitypes));
        }

        retVal = sb.toString();
        retVal = (retVal.length() > 1) ? retVal.substring(0, retVal.length() - 1) : ""; // Delete last comma
        logItOut("List from HTML: " + retVal, 2);

        return retVal;
    }

    static String resolveSourceURL(FadownParameters params) {
        String retVal = params.getFlashAirURL();

        String up = retVal.toUpperCase();
        if ((up.equals("NONE/DCIM") || up.equals("DETECT/DCIM") || up.length() == 0)) {
            retVal = searchFlashAirIPAddress() + "/DCIM";
            params.setFlashAirURL(retVal);
        }

        return retVal;
    }

    private static String scanIPs(InetAddress ipAddress) {
        String retVal = "";

        byte[] ip = ipAddress.getAddress();
        if (!ipAddress.toString().contains("127.0.0.1") && (ip.length == 4)) {
            logItOut("Scanning starts at IP address " + ipAddress.toString().replaceAll("/",""), 1);
            for (int i = 1; i <= 254; i++) {
                ip[3] = (byte)i;
                try {
                    InetAddress address = InetAddress.getByAddress(ip);
                    String htmlFromIP = readHtmlFromUrl("http:/" + address.toString(), false);
                    if (htmlFromIP.toUpperCase().contains(">FLASHAIR<")) {
                        retVal = address.toString().replaceAll("/","");
                        logItOut("FlashAir Card on IP address " + retVal, 1);
                        break;
                    }
                } catch (UnknownHostException ex) {
                }
            }
        }

        return retVal;
    }

    // Scan IP-Ranges for FlashAir Card if URL was not given
    private static String searchFlashAirIPAddress() {
        String retVal = "";

        logItOut("Scanning for FlashAir Card", 1);
        
        try {
            Enumeration e = NetworkInterface.getNetworkInterfaces();
            while (e.hasMoreElements() && retVal.equals("")) {
                NetworkInterface n = (NetworkInterface) e.nextElement();
                Enumeration ee = n.getInetAddresses();
                while (ee.hasMoreElements() && retVal.equals("")) {
                    InetAddress ipStart = (InetAddress) ee.nextElement();
                    retVal = scanIPs(ipStart);
                }
            }
        } catch (SocketException ex) {
        }

        return retVal;
    }
}
