# FADown
FADown is a Java utility to automatically download images via WiFi from a Toshiba FlashAir SD-Card inside a digital camera. Each new image is transferred within 1 to 3 seconds to the computer.

FADown Features
-
* Supports Toshiba FlashAir SD-Card 2, 3 and 4
* Transfers automatically all files of certain file types from the card to the local storage of the computer
* By default JPG is set, so only JPG files will be transferred
* Traverses all image directories below the DCIM directory
* Already transferred files will not be downloaded again. Detection based on filename
* Can be run once or continuously
* No visible window, command line utility
* Will return the number of transferred files as return code
* Able to show latest or all image files in web browser or image viewer
* Generate and show enlarged faces or eyes automatically to check sharpness
* Multiple image backups into different drives and folders
* Shooting to a layout with overlay support (PNG overlays only)
* Limit the view, face detection and overlay functions to certain file types
* Execute external program on each downloaded image
* Autodetect card if url = NONE or DETECT
* Internal viewer option
* Star ratings in csv file

Internal viewer keys
-
* Escape = Exit
* Left = scroll thru images left
* Right = scroll thru images right
* F1 = file info
* F5 = Start external program
* F6 = Start external program
* F7 = Start external program
* F8 = Start external program
* 0 = remove star rating
* 1 = rate image 1 star
* 2 = rate image 2 star
* 3 = rate image 3 star
* 4 = rate image 4 star
* 5 = rate image 5 star

(Image ratings are written to the file ratings.csv in the temp folder)

Installation
-
The file FADown.jar can be stored in an arbitrary directory and executed there.  Command line parameters see below.<br>
For face/eye detection openCV must be installed (https://opencv.org/releases.html).


Command line parameters
-
| Short option | Long option | Parameter value | Description |
| ------ | ------ | ------ | ------ |
| -u | --url | url | URL of the FlashAir card, url = NONE or DETECT initiates search for card
| -d | --destination-directory | directory | Destination directory
| -o | --backup-folders | folder names list | Backup files to folders, comma separated list
| -f | --filetypes | file extension list | Comma separated list of file extensions
| -n | --png-overlay | png filename | Overlay every image with the png image
| -e | --handle | file extension list | Process (view, detection, overlay) only certain file extensions, comma separated list
| -t | --external-program | program and parameter list | Start external program, image filename will be added as last parameter
| -c | --continuously | | Run continuously, waiting for more files
| -r | --parallel | | Parallel download files
| -p | --pause | seconds | Wait seconds before try again in continuous mode
| -w | --wait | seconds  | Wait seconds after downloading a file to prevent overheating
| -i | --image-viewer | | Show images in internal viewer
| -5 | --f5-external | program and parameter list | Start external program on F5 key, image filename will be added as last parameter (internal viewer only)
| -6 | --f6-external | program and parameter list | Start external program on F6 key, image filename will be added as last parameter (internal viewer only)
| -7 | --f7-external | program and parameter list | Start external program on F7 key, image filename will be added as last parameter (internal viewer only)
| -8 | --f8-external | program and parameter list | Start external program on F8 key, image filename will be added as last parameter (internal viewer only)
| -a | --all | | Show all images in browser
| -b | --browser | | Show last image in browser
| -l | --log | | Log transferred filenames on console
| -x | --xlog | | Extended logging on console
| -s | --stop | | Stop all instances of FADown on this computer
| -v | --view | filename list | Show image files, comma separated list
| -q | --faces | pixel | Show faces in separate images of pixel size
| -y | --eyes | pixel | Show eyes in separate images of pixel size
| -g | --generate-detection | | Do not show detected features images, only generate them
| -k | --config | filename | Load parameters from config file (default: FADown.cfg)
| -h | --help | | Display help

**Examples:**<br>
java -jar FADown.jar -u DETECT -d D:\FADOWN\IMAGES -f JPG -c<br>
java -jar FADown.jar -u FLASHAIR -d D:\FADOWN\IMAGES -f JPG -c<br>
java -jar FADown.jar -u FLASHAIR -d D:\FADOWN\IMAGES -f JPG -c -i<br>
java -jar FADown.jar --url FLASHAIR --destination-directory D:\FADOWN\IMAGES --filetypes JPG,CR2 --handle JPG -log<br>
java -Djava.library.path=C:\opencv\build\java\x64 -jar FADown.jar -u FLASHAIR -d D:\FADOWN\IMAGES -f JPG -y 1000<br>
java -Djava.library.path=C:\opencv\build\java\x64 -jar FADown.jar -v "C:\Bilder\bild 1.jpg" -y 1000<br>
java -Djava.library.path=C:\opencv\build\java\x64 -jar FADown.jar -v C:\Bilder\bild1.jpg,C:\Bilder\bild2.jpg -y 1000<br>
java -jar FADown.jar -u FLASHAIR -d D:\TEMP\FAIMGs -f JPG -l -c -a -n C:\Bilder\Overlay_fashion_x_cover.png<br>
java -jar FADown.jar -u FLASHAIR -d D:\FADOWN\IMAGES -f JPG -c -t "extra_script.cmd param_A param_B" <br>
java -jar FADown.jar --stop<br>

**Example config file:**<br>
url=FLASHAIR<br>
destination-directory=C:\TEMP\img_destination<br>
filetypes=JPG<br>
external-program=store_image_in_database.cmd img_databasename<br>
continuously<br>
pause=10<br>
log<br>

If started with no command line parameters at all, FADown will use FLASHAIR as the URL, JPG as filetypes and will store the downloaded files in a directory named FA_IMGs in the temp directory of the user. 
This directory will be automatically created on startup. Filenames will be logged.

Hints
-
* If there is only the TSB directory on the card any new file will be stored there. FADown ignores this directory. Best practice is to define the DCIM directory manually on the card (via card reader).
* The WiFi range of this SD-Cards is fairly limited. The range can be extended by using a pocket router that runs from rechargeable batteries. The computer and the card can be connected to this router that should be carried near the camera (shirt pocket etc.). The FlashAir card should be configured for APPMODE=5.
* How long takes a transfer? With the FlashAir 4 transfer times of 2 seconds per JPG are possible. Set the camera to RAW + smallest JPG. In burst mode 1 seconds and less are possible (depending on the camera).
* The FlashAir SD-Card can overheat if too much files are transferred in a short time without a break. So it is possible to slow down the download process by using the --wait and/or the --pause command line options.
* For the face/eye detection and enlarging feature openCV must be installed (https://opencv.org/releases.html). The path to the openCV library had to be set for the JVM. Example: java -Djava.library.path=C:\opencv\build\java\x64 -jar FADown.jar ...
* If config file specified (or default FADown.cfg exists), all other command line parameters are ignored.
* Using the --parallel parameter will download the files not necessarily in consecutive order. Can be problematic if an external viewer is used.
* Images with png overlay (-n parameter) will be written without any EXIF/IPTC metadata.

# FADown UI
For FADown a simple UI via fadown_ui.hta is available in the project structure. It is possilbe to fill each command line parameter of FADown with a value and to start this command line.
Additionally there are buttons for command line help, stopping all running FADown instances and displaying the FADown web site.

# FADown Simple Menu
fadown_demo_menu.hta contains a simple menu system to start certain FADown configurations via command line.
Simply change the block below "Change only from here till the end of the menu" by adding, changing or deleting a menu line.
Each menu line consists of an input button, a value which contains the menu caption and an onclick event that starts the command line.
